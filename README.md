# GoPro

[Go Pro hilight tags](https://superuser.com/a/884727/490452)

[Wi-Fi Name and Password reset](https://gopro.com/help/articles/Block/How-to-Reset-the-Camera-s-Wi-Fi-Name-and-Password), default password `goprohero`

[GoPro Hacks Issue](https://github.com/KonradIT/goprowifihack/issues/112)

[GoPro as webcam](https://www.youtube.com/watch?v=BAWbv3R5qbY)

[The pairing code:](https://github.com/KonradIT/gopro-py-api/blob/master/goprocam/GoProCamera.py#L362)

```python
def pair(self, usepin=True):
	#This is a pairing procedure needed for HERO4 and HERO5 cameras. When those type GoPro camera are purchased the GoPro Mobile app needs an authentication code when pairing the camera to a mobile device for the first time. 
	#The code is useless afterwards. This function will pair your GoPro to the machine without the need of using the mobile app -- at all.
	if usepin == False:
		paired_resp = ""
		while "{}" not in paired_resp:
			paired_resp = urllib.request.urlopen('http://' + self.ip_addr + '/gp/gpControl/command/wireless/pair/complete?success=1&deviceName=' + socket.gethostname(), timeout=5).read().decode('utf8')
		print("Paired")
		return
	else:
		print("Make sure your GoPro camera is in pairing mode!\nGo to settings > Wifi > PAIR > GoProApp to start pairing.\nThen connect to it, the ssid name should be GOPRO-XXXX/GPXXXXX/GOPRO-BP-XXXX and the password is goprohero")
		code=str(input("Enter pairing code: "))
		_context = ssl._create_unverified_context()
		ssl._create_default_https_context = ssl._create_unverified_context
		response_raw = urllib.request.urlopen('https://' + self.ip_addr + '/gpPair?c=start&pin=' + code + '&mode=0', context=_context).read().decode('utf8')
		print(response_raw)
		response_raw = urllib.request.urlopen('https://' + self.ip_addr + '/gpPair?c=finish&pin=' + code + '&mode=0', context=_context).read().decode('utf8')
		print(response_raw)
		wifi_ssid=input("Enter your desired camera wifi ssid name: ")
		wifi_pass=input("Enter new wifi password: ")
		self.gpControlCommand("wireless/ap/ssid?ssid=" + wifi_ssid + "&pw=" + wifi_pass)
print("Connect now!")
```
